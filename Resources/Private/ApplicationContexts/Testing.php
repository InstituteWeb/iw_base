<?php

$GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = false;
$GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = false;

$GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '*';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = 1;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = '';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = 0;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLogLevel'] = 1;

$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport'] = 'mail';
