<?php

$GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = false;
$GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = false;

$GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '87.79.12.40';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = 2;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = '';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = 0;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLogLevel'] = 1;

$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport'] = 'smtp';
