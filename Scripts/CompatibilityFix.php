<?php

$sitePath = realpath(__DIR__ . '/../../../..') . DIRECTORY_SEPARATOR;
require_once($sitePath . '/vendor/autoload.php');

$installer = new \InstituteWeb\IwBase\Scripts\CompatibilityFix($sitePath);
$installer->run();
