<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2014-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$boot = function ($extensionKey) {
    if (TYPO3_MODE === 'BE') {
        $extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$extensionKey]);

        $GLOBALS['TCA']['backend_layout']['ctrl']['readOnly'] = true;
        $GLOBALS['TCA']['tx_gridelements_backend_layout']['ctrl']['readOnly'] = true;

        // Add itemsProcFunc to all select boxes, which do not have a func yet.
        if ($extensionConfiguration['enableRespectAccessInSelectFilter']) {
            foreach ($GLOBALS['TCA'] as $table => $configuration) {
                if (isset($configuration['columns'])) {
                    foreach ($configuration['columns'] as $name => $fieldConfiguration) {
                        if ($fieldConfiguration['config']['type'] === 'select' &&
                            !isset($fieldConfiguration['config']['itemsProcFunc'])) {
                            $GLOBALS['TCA'][$table]['columns'][$name]['config']['itemsProcFunc'] =
                                'InstituteWeb\IwBase\UserFunctions\RespectAccessInSelect->filter';
                        }
                    }
                }
            }
        }

        $extensionPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extensionKey);
        if (\TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->isDevelopment()) {
            $GLOBALS['TBE_STYLES']['logo'] = $extensionPath . 'Resources/Public/Images/typo3-topbar-dev@2x.png';
        } elseif (\TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->isTesting()) {
            $GLOBALS['TBE_STYLES']['logo'] = $extensionPath . 'Resources/Public/Images/typo3-topbar-test@2x.png';
        }
    }

    /** @var \InstituteWeb\IwBase\DomainHandling\DomainController $domainController */
    $domainController = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        'InstituteWeb\IwBase\DomainHandling\DomainController'
    );
    $domainController->runAction();
    $GLOBALS['TCA']['sys_domain']['ctrl']['readOnly'] = '1';
};

$boot($_EXTKEY);
unset($boot);
