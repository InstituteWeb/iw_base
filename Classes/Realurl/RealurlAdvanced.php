<?php
namespace InstituteWeb\IwBase\Realurl;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is free  *
 *  | software and licensed under GNU General Public License.                 *
 *  |                                                                         *
 *  | (c) 2014-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>                 */

use TYPO3\CMS\Core\Utility\GeneralUtility;

if (!class_exists('\tx_realurl_advanced')) {
    require_once(GeneralUtility::getFileAbsFileName('EXT:realurl/class.tx_realurl_advanced.php'));
}

/**
 * Extends realurl advanced to respect special options
 * in realurl configuration (realurl_conf.php)
 *
 * @package InstituteWeb\IwBase
 */
class RealurlAdvanced extends \tx_realurl_advanced
{

    /**
     * Add new option to fileName section in realurl_conf.php.
     * If keyValues contain key 'jumpToPageUid' this page will be called
     * instead of current root page. Example:
     *
     * 'jumpToPageUid' => 2
     *
     * @param array $pathParts
     * @return int
     */
    protected function pagePathtoID(&$pathParts)
    {
        $returnValue = parent::pagePathtoID($pathParts);
        foreach ($this->extConf['fileName']['index'] as $fileName => $config) {
            $uri = GeneralUtility::getIndpEnv('REQUEST_URI');
            if ($fileName === substr($uri, strlen($fileName) * -1) && isset($config['keyValues']['jumpToPageUid'])) {
                $returnValue[0] = (int)$config['keyValues']['jumpToPageUid'];
            }
        }
        return $returnValue;
    }
}
