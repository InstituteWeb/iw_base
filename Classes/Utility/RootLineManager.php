<?php
namespace InstituteWeb\IwBase\Utility;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Root line fields utility
 *
 * @package InstituteWeb\IwBase
 */
class RootLineManager
{
    /**
     * Extends current "addRootLineFields" by given, comma separated string.
     * $GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields']
     *
     * @param string $rootLineFieldsToAdd Comma separated list of new root line fields
     * @return void
     */
    public static function addFields($rootLineFieldsToAdd)
    {
        $fieldsToAdd = GeneralUtility::trimExplode(',', $rootLineFieldsToAdd, true);
        $currentRootLineFields = GeneralUtility::trimExplode(
            ',',
            $GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'],
            true
        );

        // Check if given fields are existing in database
        $databaseConnection = DatabaseConnection::get();
        $validFieldsToAdd = array_intersect($fieldsToAdd, array_keys($databaseConnection->admin_get_fields('pages')));

        $GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields']
            = ',' . implode(',', array_merge($currentRootLineFields, $validFieldsToAdd));
    }
}
