<?php
namespace InstituteWeb\IwBase\Utility;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2014-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */

/**
 * Class to get speaking url from realurl out of any TYPO3 context
 *
 * @package InstituteWeb\IwBase
 */
class Database
{
    /**
     * @var array
     */
    protected $databaseConfiguration = array();

    /**
     * @var null|\mysqli
     */
    protected $database = null;

    /**
     * @var boolean
     */
    protected $isConnected = false;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->initialize();
    }

    /**
     * Initializes database and realurl configuration
     *
     * @return void
     */
    protected function initialize()
    {
        $typoDatabaseHost = $typoDatabaseUsername = $typoDatabasePassword = $typoDatabaseName = null;

        if (defined('TYPO3_db_host') && defined('TYPO3_db_username') && defined('TYPO3_db_password') &&
            defined('TYPO3_db')) {
            $typoDatabaseHost = TYPO3_db_host;
            $typoDatabaseUsername = TYPO3_db_username;
            $typoDatabasePassword = TYPO3_db_password;
            $typoDatabaseName = TYPO3_db;
        } else {
            error_reporting(E_ERROR | E_WARNING | E_PARSE);
            require_once(PATH_site . 'typo3conf/AdditionalConfiguration.php');
            if (empty($typoDatabaseHost)) {
                $typoDatabaseHost = $GLOBALS['TYPO3_CONF_VARS']['DB']['host'];
            }
            if (empty($typoDatabaseUsername)) {
                $typoDatabaseUsername = $GLOBALS['TYPO3_CONF_VARS']['DB']['username'];
            }
            if (empty($typoDatabasePassword)) {
                $typoDatabasePassword = $GLOBALS['TYPO3_CONF_VARS']['DB']['password'];
            }
            if (empty($typoDatabaseName)) {
                $typoDatabaseName = $GLOBALS['TYPO3_CONF_VARS']['DB']['database'];
            }
        }

        $this->databaseConfiguration = array(
            'host' => $typoDatabaseHost,
            'username' => $typoDatabaseUsername,
            'password' => $typoDatabasePassword,
            'database' => $typoDatabaseName
        );

        $this->database = new \mysqli(
            $this->databaseConfiguration['host'],
            $this->databaseConfiguration['username'],
            $this->databaseConfiguration['password'],
            $this->databaseConfiguration['database']
        );

        if ($this->database->connect_errno) {
            $this->isConnected = false;
        } else {
            $this->isConnected = true;
        }
    }

    /**
     * Executes given query
     *
     * @param string $query
     * @return \mysqli_result
     * @throws \Exception
     */
    public function executeQuery($query)
    {
        $result = $this->database->query($query);
        if ($result === false && $this->database->errno) {
            throw new \Exception('Query failed: "' . $query . '" with message: ' . $this->database->error, 1399300611);
        }
        return $result;
    }

    /**
     * Escapes given string
     *
     * @param string $string
     * @return string
     */
    public function escapeString($string)
    {
        return $this->database->escape_string($string);
    }

    /**
     * @param string $query
     * @return array assoc array with results
     */
    public function getRows($query)
    {
        $result = array();
        $queryResult = $this->executeQuery($query);
        if ($queryResult instanceof \mysqli_result) {
            while (($row = $queryResult->fetch_assoc())) {
                $result[] = $row;
            }
        }
        return $result;
    }

    /**
     * @param string $query
     * @return array assoc array with result (single row, direct attributes)
     */
    public function getSingleRow($query)
    {
        return $this->executeQuery($query)->fetch_assoc();
    }

    /**
     * @return boolean
     */
    public function isConnected()
    {
        return $this->isConnected;
    }
}
