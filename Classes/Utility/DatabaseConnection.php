<?php
namespace InstituteWeb\IwBase\Utility;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */

/**
 * DatabaseConnection Utility
 *
 * @package InstituteWeb\IwBase
 */
class DatabaseConnection
{
    /**
     * Returns current DatabaseConnection or creates one, if not existing
     *
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    public static function get()
    {
        if (!$GLOBALS['TYPO3_DB']) {
            \TYPO3\CMS\Core\Core\Bootstrap::getInstance()->initializeTypo3DbGlobal();
        }
        return $GLOBALS['TYPO3_DB'];
    }
}
