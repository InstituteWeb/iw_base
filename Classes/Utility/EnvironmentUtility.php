<?php
namespace InstituteWeb\IwBase\Utility;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2014-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */
use InstituteWeb\IwBase\DomainHandling\DomainUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Helper class for different environments based on ApplicationContexts
 *
 * @package Iwm\IwBase
 */
class EnvironmentUtility
{
    /** @var array */
    static protected $TYPO3_CONF_VARS = array();

    /**
     * Initializes this utilities
     *
     * @param array $TYPO3_CONF_VARS
     * @return void
     */
    public static function init(&$TYPO3_CONF_VARS)
    {
        self::$TYPO3_CONF_VARS = &$TYPO3_CONF_VARS;
        if (!isset($GLOBALS[DomainUtility::SECTION][DomainUtility::EXTKEY]['domainConfigurations'])) {
            $GLOBALS[DomainUtility::SECTION][DomainUtility::EXTKEY]['domainConfigurations'] = array();
        }
    }

    /**
     * Includes environment settings based on root application context
     * located in EXT:iw_base/Resources/Private/ApplicationContexts/
     *
     * Also it appends the root application context name to ['SYS']['sitename']
     * wrapped with squared brackets.
     *
     * @return void
     */
    public static function includeRootEnvironmentSettings()
    {
        $rootContext = static::getRootApplicationContext();
        $extPath = PATH_typo3conf . 'ext/iw_base/';

        $includePath = $extPath . 'Resources/Private/ApplicationContexts/' . (string) $rootContext . '.php';
        require_once($includePath);

        $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] .= ' [' . EnvironmentUtility::getRootApplicationContext() . ']';
    }

    /**
     * Includes sub environment settings based on sub application contexts
     * which are located in typo3conf/Environments/
     *
     * Examples:
     * The context "Development/DevServer" will include "typo3conf/Environments/DevServer.php"
     * The context "Development/Live/Server1" will include "typo3conf/Environments/Live/Server1.php"
     *
     * @return void
     */
    public static function includeSubEnvironmentSettings()
    {
        $applicationContext = GeneralUtility::getApplicationContext();
        $contextParts = GeneralUtility::trimExplode('/', (string) $applicationContext, true);
        array_shift($contextParts);
        $subContext = implode('/', $contextParts);
        $subEnvironmentPath = PATH_typo3conf . 'Environments/' . $subContext . '.php';
        if (file_exists($subEnvironmentPath)) {
            require_once($subEnvironmentPath);
        }
    }


    /**
     * Returns root ApplicationContext (Development, Production or Testing)
     *
     * @return \TYPO3\CMS\Core\Core\ApplicationContext The root context
     */
    public static function getRootApplicationContext()
    {
        $context = GeneralUtility::getApplicationContext();
        do {
            $rootContext = $context;
            $context = $context->getParent();
        } while (!is_null($context));
        return $rootContext;
    }

    /**
     * Compares given contextPattern with current application context
     *
     * @param string $contextPattern Simple string (with wildcard * support) or regular expression.
     *                               You can also pass multiple patterns, separated by comma.
     * @return bool Returns true if given pattern matches with current application context
     */
    public static function matchApplicationContext($contextPattern)
    {
        $values = GeneralUtility::trimExplode(',', $contextPattern, true);
        foreach ($values as $applicationContext) {
            if (static::searchStringWildcard(GeneralUtility::getApplicationContext(), $applicationContext)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Matching two strings against each other, supporting a "*" wildcard
     * or (if wrapped in "/") PCRE regular expressions
     *
     * @param string $haystack
     * @param string $needle
     * @return bool
     *
     * @see \TYPO3\CMS\Core\Configuration\TypoScript\ConditionMatching\AbstractConditionMatcher::searchStringWildcard
     */
    protected static function searchStringWildcard($haystack, $needle)
    {
        $result = false;
        if ($haystack === $needle) {
            $result = true;
        } elseif ($needle) {
            if (preg_match('/^\\/.+\\/$/', $needle)) {
                // Regular expression, only "//" is allowed as delimiter
                $regex = $needle;
            } else {
                $needle = str_replace(array('*', '?'), array('###MANY###', '###ONE###'), $needle);
                $regex = '/^' . preg_quote($needle, '/') . '$/';
                // Replace the marker with .* to match anything (wildcard)
                $regex = str_replace(array('###MANY###', '###ONE###'), array('.*', '.'), $regex);
            }
            $result = (bool)preg_match($regex, $haystack);
        }
        return $result;
    }

    /**
     * Set credentials for TYPO3 DatabaseConnection
     *
     * @param string $username
     * @param string $password
     * @param string $database
     * @param string $host
     * @return void
     */
    public static function setDatabaseCredentials($username, $password, $database, $host = '127.0.0.1')
    {
        $GLOBALS['TYPO3_CONF_VARS']['DB']['host'] = $host;
        $GLOBALS['TYPO3_CONF_VARS']['DB']['username'] = $username;
        $GLOBALS['TYPO3_CONF_VARS']['DB']['password'] = $password;
        $GLOBALS['TYPO3_CONF_VARS']['DB']['database'] = $database;
    }

    /**
     * Sets given setting of extension configuration
     *
     * @param string $extensionKey
     * @param string $settingName
     * @param mixed $value
     * @return void
     */
    public static function setExtensionSetting($extensionKey, $settingName, $value)
    {
        $extensionConfiguration = array();
        if (isset(self::$TYPO3_CONF_VARS['EXT']['extConf'][$extensionKey])) {
            $extensionConfiguration = unserialize(self::$TYPO3_CONF_VARS['EXT']['extConf'][$extensionKey]);
            if (is_array($extensionConfiguration)) {
                $extensionConfiguration[$settingName] = $value;
            }
        } else {
            $extensionConfiguration[$settingName] = $value;
        }
        if (is_array($extensionConfiguration)) {
            self::$TYPO3_CONF_VARS['EXT']['extConf'][$extensionKey] = serialize($extensionConfiguration);
        }
    }

    /**
     * Adds given extensionKey to runtimeActivatedPackages
     *
     * @param string $extensionKey
     * @return void
     */
    public static function activateExtension($extensionKey)
    {
        if (!is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['runtimeActivatedPackages'])) {
            $GLOBALS['TYPO3_CONF_VARS']['EXT']['runtimeActivatedPackages'] = array();
        }
        if (!in_array($extensionKey, $GLOBALS['TYPO3_CONF_VARS']['EXT']['runtimeActivatedPackages'])) {
            $GLOBALS['TYPO3_CONF_VARS']['EXT']['runtimeActivatedPackages'][] = $extensionKey;
        }
    }

    /**
     * Get domain utility which may get used to set domain configurations for environments.
     *
     * @return DomainUtility
     * @throws \Exception
     */
    public static function getDomainConfiguration()
    {
        $className = 'InstituteWeb\IwBase\DomainHandling\DomainUtility';
        $filePath = PATH_typo3conf . 'ext/iw_base/Classes/DomainHandling/DomainUtility.php';
        if (!class_exists($className) && file_exists($filePath)) {
            require_once($filePath);
        }
        if (!class_exists($className)) {
            throw new \Exception(
                'To be able to use "getDomainConfiguration()" the extension iw_base must be installed!'
            );
        }
        return GeneralUtility::makeInstance($className);
    }
}
