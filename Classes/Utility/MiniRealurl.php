<?php
namespace InstituteWeb\IwBase\Utility;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2014-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */

/**
 * Class to get speaking url from realurl out of any TYPO3 context
 *
 * @package InstituteWeb\IwBase
 */
class MiniRealurl
{
    /**
     * @var Database
     */
    protected $database = null;

    /**
     * @var array
     */
    protected $realUrlConfiguration = array();

    /**
     * Construct
     */
    public function __construct()
    {
        $this->initialize();
    }

    /**
     * Initializes database and realurl configuration
     *
     * @return void
     */
    protected function initialize()
    {
        require_once(PATH_site . 'typo3conf/ext/iw_base/Classes/Utility/Database.php');
        $this->database = new Database();

        require_once(PATH_site . 'typo3conf/realurl_conf.php');
        $this->realUrlConfiguration = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'];

        if ($this->realUrlConfiguration === null) {
            $this->realUrlConfiguration = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'];
        }
    }

    /**
     * Gets realUrl configuration value of first level. Keys on first level
     * are eg.: enableDomainLookup, init, preVars, pagePath, fileName,
     * fixedPostVarSets, postVarSets
     *
     * @param string $key Key of first level
     * @return mixed Value of given key. Mostly an array.
     */
    protected function getRealUrlConfigurationValue($key)
    {
        if (array_key_exists($_SERVER['HTTP_HOST'], $this->realUrlConfiguration)) {
            return $this->realUrlConfiguration[$_SERVER['HTTP_HOST']][$key];
        }
        return $this->realUrlConfiguration['_DEFAULT'][$key];
    }

    /**
     * Get speaking URL from realUrl cache by given page id and sys_language_uid.
     *
     * @param int $pageId
     * @param int $sysLanguageUid
     * @return string Encoded speaking URL. If no entries found, returns a
     *                fallback with working but without speaking URL.
     */
    public function encodeUrl($pageId, $sysLanguageUid)
    {
        $fallbackUri = '?id=' . $pageId . '&L=' . $sysLanguageUid;

        if (!$this->database->isConnected()) {
            return $fallbackUri;
        }

        $result = $this->database->executeQuery(
            'SELECT pagepath FROM tx_realurl_pathcache ' .
            'WHERE page_id = ' . $pageId . ' AND language_id = ' . $sysLanguageUid
        );
        if ($result->num_rows === 0) {
            return $fallbackUri;
        }
        $pagePath = current($result->fetch_assoc());

        $preVars = $this->getRealUrlConfigurationValue('preVars');
        if (isset($preVars['L']) && isset($preVars['L']['valueMap'])) {
            $languageValueMap = array_flip($preVars['L']['valueMap']);
        } else {
            return $fallbackUri;
        }

        if (isset($languageValueMap[$sysLanguageUid])) {
            $speakingUrl = $languageValueMap[$sysLanguageUid] . '/' . $pagePath;
        } else {
            $speakingUrl = $pagePath;
        }

        $fileName = $this->getRealUrlConfigurationValue('fileName');
        if ($fileName['defaultToHTMLsuffixOnPrev']) {
            $speakingUrl .= '.html';
        }
        return $speakingUrl;
    }
}
