<?php
namespace InstituteWeb\IwBase\ViewHelpers\Uri;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2014-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Extends fluid's viewhelper f:uri.resource
 *
 * Usage example:
 * {f:uri.resource(path:'Custom/Images/Logo.png', section:'Default', extensionName:'IwAssets')}
 *
 * Or:
 * {namespace iw=InstituteWeb\IwBase\ViewHelpers}
 * {iw:uri.resource(path:'Custom/Images/Logo.png', section:'Default', extensionName:'IwAssets')}
 *
 * @package InstituteWeb\IwBase
 */
class ResourceViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Uri\ResourceViewHelper
{
    /**
     * Render the URI to the resource. The filename is used from child content.
     *
     * @param string $path The path and filename of the resource relative to Public resource directory of the extension)
     * @param string $extensionName Target extension name. If not set, the current extension name will be used
     * @param bool $absolute If set, an absolute URI is rendered
     * @param string|null $section Add section to path name, if given
     * @return string The URI to the resource
     */
    public function render($path, $extensionName = null, $absolute = false, $section = null)
    {
        return static::renderStatic(
            array(
                'path' => $path,
                'extensionName' => $extensionName,
                'absolute' => $absolute,
                'section' => $section
            ),
            $this->buildRenderChildrenClosure(),
            $this->renderingContext
        );
    }

    /**
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     */
    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $path = $arguments['path'];
        $extensionName = $arguments['extensionName'];
        $absolute = $arguments['absolute'];
        $section = $arguments['section'];

        if ($extensionName === null) {
            $extensionName = $renderingContext->getControllerContext()->getRequest()->getControllerExtensionName();
        }

        $resourcesFolder = '/Resources/Public/';
        if ($section !== null) {
            $resourcesFolder = '/Resources/' . $section . '/Public/';
        }

        $uri = 'EXT:' . GeneralUtility::camelCaseToLowerCaseUnderscored($extensionName) . $resourcesFolder . $path;
        $uri = GeneralUtility::getFileAbsFileName($uri);
        $uri = \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix($uri);
        if (TYPO3_MODE === 'BE' && $absolute === false && $uri !== false) {
            $uri = '../' . $uri;
        }
        if ($absolute === true) {
            $uri = $renderingContext->getControllerContext()->getRequest()->getBaseUri() . $uri;
        }
        return $uri;
    }
}
