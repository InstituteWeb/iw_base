<?php
namespace InstituteWeb\IwBase\UserFunctions;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2014-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */
use InstituteWeb\IwBase\Utility\DatabaseConnection;
use TYPO3\CMS\Backend\Form\FormDataProviderInterface;

/**
 * Class RespectAccessInSelect
 *
 * @package InstituteWeb\IwBase
 */
class RespectAccessInSelect
{
    /**
     * Removes items which are not accessible by current backend user
     *
     * @param array $params Parameters as referenced array
     * @param FormDataProviderInterface $formEngine
     * @return void
     */
    public function filter(array &$params, FormDataProviderInterface $formEngine)
    {
        if (!empty($params['items']) && isset($params['config']['foreign_table'])) {
            if ($this->tableHasPid($params['config']['foreign_table'])) {
                foreach ($params['items'] as $key => $values) {
                    $uid = intval($values[1]);
                    $itemRow = $this->getDatabaseConnection()->exec_SELECTgetSingleRow(
                        'pid',
                        $params['config']['foreign_table'],
                        'uid=' . $uid
                    );
                    if (is_array($itemRow) && isset($itemRow['pid'])) {
                        $pageRow = $this->getDatabaseConnection()->exec_SELECTgetSingleRow(
                            '*',
                            'pages',
                            'uid=' . $itemRow['pid']
                        );
                        /** @var \TYPO3\CMS\Core\Authentication\BackendUserAuthentication $backendUser */
                        $backendUser = $GLOBALS['BE_USER'];
                        $userHasAccess = $backendUser->doesUserHaveAccess($pageRow, 1);
                        if (!$userHasAccess) {
                            unset($params['items'][$key]);
                        }
                    }
                }
            }
        }
    }

    /**
     * Checks if given table has the column pid
     *
     * @param string $tableName
     * @return boolean
     */
    protected function tableHasPid($tableName)
    {
        $fields = $this->getDatabaseConnection()->admin_get_fields($tableName);
        return array_key_exists('pid', $fields);
    }

    /**
     * Returns current database connection
     *
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected function getDatabaseConnection()
    {
        return DatabaseConnection::get();
    }
}
