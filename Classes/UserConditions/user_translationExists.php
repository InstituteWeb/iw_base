<?php
namespace InstituteWeb\IwBase\UserConditions;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2014-2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * Checks if the current page in frontend has a translation.
 *
 * Usage:
 * [userFunc = \InstituteWeb\IwBase\UserConditions\user_translationExists()]
 *  && [globalVar = GP:L > 0]
 *
 * @return bool Returns TRUE if the current page has been translated
 */
function user_translationExists()
{
    if (TYPO3_MODE !== 'FE' || GeneralUtility::_GP('eID')) {
        return null;
    }

    /** @var $contentObjectRenderer ContentObjectRenderer */
    $contentObjectRenderer = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
    $sysLanguageUid = (int)GeneralUtility::_GP('L');
    $pageUid = intval($GLOBALS['TSFE']->id);

    $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
        'uid',
        'pages_language_overlay',
        'pid = ' . $pageUid . ' AND sys_language_uid = ' . $sysLanguageUid .
        $contentObjectRenderer->enableFields('pages_language_overlay')
    );
    return (bool)$GLOBALS['TYPO3_DB']->sql_num_rows($res);
}
