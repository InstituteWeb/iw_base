<?php
namespace InstituteWeb\IwBase\Scripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */
use TYPO3\CMS\Extbase\Mvc\Cli\ConsoleOutput;

/**
 * Class Install Script
 *
 * @package InstituteWeb\IwBase
 */
class Install
{
    /**
     * @var string Absolute root path of TYPO3
     */
    protected $PATH_site;

    /**
     * @var ConsoleOutput
     */
    protected $output;

    /**
     * @var array Entered user data
     */
    protected $userData = array();

    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * Install constructor.
     *
     * @param string $pathSite Absolute root path of TYPO3. Required.
     * @return Install
     */
    public function __construct($pathSite)
    {
        $this->PATH_site = $pathSite;
        $this->output = new ConsoleOutput();
    }


    public function run()
    {
        $this->showTitle();
        $this->checkApplicationContext();
        $this->setTypo3ContextInHtaccess();
        $this->checkEnvironmentConfigurationFile();

        $this->checkForDatabaseCredentialsInEnvironmentConfiguration();
        $this->promptDatabaseCredentials();
        $this->establishDatabaseConnection();
        $this->writeDatabaseCredentialsToEnvironmentConfiguration();

        $this->checkDatabaseForExistingTables();
        $this->importBasicSchema();
        $this->createCliLowlevelBackendUser();

        $this->performUpdateSchema();
        $this->fixFolderStructure();
        $this->clearTypo3Caches();

        $this->performT3lephantImport();

        $this->output->outputLine('Ready.');
    }

    protected function showTitle()
    {
        $this->output->outputLine('+---------------------------+');
        $this->output->outputLine('|  iw_base TYPO3 Installer  |');
        $this->output->outputLine('+---------------------------+');
        $this->output->outputLine();
    }

    protected function checkApplicationContext()
    {
        if (!isset($_SERVER['TYPO3_CONTEXT'])) {
            $htaccessFiles = $this->getHtaccessFiles();
            foreach ($htaccessFiles as $file) {
                if (!empty($file['context'])) {
                    $_SERVER['TYPO3_CONTEXT'] = $file['context'];
                    $this->userData['contextFromHtaccess'] = $file;
                    $this->output->outputLine('Found TYPO3_CONTEXT in .htaccess file ' . $file['file']);
                    $this->output->outputLine('Set TYPO3_CONTEXT for this script to "' . $file['context'] . '".');
                    $this->output->outputLine();
                    return;
                }
            }

            $this->output->outputLine('The environment variable TYPO3_CONTEXT is empty but required.');
            $_SERVER['TYPO3_CONTEXT'] = $this->output->ask(
                'Set TYPO3_CONTEXT [Development/DevServer]: ',
                'Development/DevServer'
            );

            $this->output->outputLine('TYPO3_CONTEXT set to "' . $_SERVER['TYPO3_CONTEXT'] . '" for this script.');
            $this->output->outputLine();
        }
    }

    /**
     * Does the same like realpath() does, but does not require that the referenced file
     * is existing.
     *
     * @param string $path
     * @return string
     */
    protected function normalizePath($path)
    {
        $parts = array();// Array to build a new path from the good parts
        $path = str_replace('\\', '/', $path);// Replace backslashes with forwardslashes
        $path = preg_replace('/\/+/', '/', $path);// Combine multiple slashes into a single slash
        $segments = explode('/', $path);// Collect path segments
        $test = '';// Initialize testing variable
        foreach ($segments as $segment) {
            if ($segment !== '.') {
                $test = array_pop($parts);
                if (is_null($test)) {
                    $parts[] = $segment;
                } else {
                    if ($segment === '..') {
                        if ($test === '..') {
                            $parts[] = $test;
                        }

                        if ($test === '..' || $test === '') {
                            $parts[] = $segment;
                        }
                    } else {
                        $parts[] = $test;
                        $parts[] = $segment;
                    }
                }
            }
        }
        return implode('/', $parts);
    }

    /**
     * Get existing and possible htaccess files
     * located from current working directory, up to root
     *
     * @return array
     */
    protected function getHtaccessFiles()
    {
        $pathPartsAmount = count(explode(DIRECTORY_SEPARATOR, trim($this->PATH_site, DIRECTORY_SEPARATOR)));
        $htaccessFiles = array();
        for ($i = 0; $i <= $pathPartsAmount; $i++) {
            $path = $this->normalizePath($this->PATH_site . str_repeat('../', $i) . '.htaccess');
            $pathDir = $this->normalizePath($this->PATH_site . str_repeat('../', $i));

            if (file_exists($path) || is_writable($pathDir)) {
                $containsContext = null;
                if (file_exists($path)) {
                    $contents = file_get_contents($path);
                    $containsContext = strpos($contents, 'TYPO3_CONTEXT');
                    $context = '';
                    if ($containsContext !== false) {
                        preg_match('/.*TYPO3_CONTEXT[\s\:\=]?(.*)/', $contents, $matches);
                        $context = trim($matches[1]);
                    }
                }
                $htaccessFiles[] = array(
                    'file' => $path,
                    'existing' => file_exists($path),
                    'dirWritable' => is_dir($pathDir) && is_writable($pathDir),
                    'recommended' => $i === 1,
                    'containsContext' => $containsContext,
                    'context' => $context
                );
            }
        }
        return $htaccessFiles;
    }

    protected function setTypo3ContextInHtaccess()
    {
        if (isset($this->userData['contextFromHtaccess'])) {
            return;
        }

        // Ask if user want to add environment variable
        $addEnvironmentVariable = $this->output->askConfirmation(
            'Do you want to add "SetEnv TYPO3_CONTEXT ' . $_SERVER['TYPO3_CONTEXT'] . '" to .htaccess? [Y/n] ',
            true
        );
        if (!$addEnvironmentVariable) {
            $this->output->outputLine(
                'Okay, but don\'t forget to set TYPO3_CONTEXT manually. Otherwise frontend will fail.'
            );
            $this->output->outputLine();
            return;
        }

        // Get available htaccess files and list them
        $i = 1;
        $htaccessFiles = $this->getHtaccessFiles();
        $this->output->outputLine();
        $this->output->outputFormatted('0) %s', array('Skip'), 3);
        foreach ($htaccessFiles as $htaccessFile) {
            $status = array();
            if ($htaccessFile['existing']) {
                $status[] = 'file exists';
            }
            if ($htaccessFile['containsContext']) {
                $status[] = 'already contains TYPO3_CONTEXT';
            }
            if ($htaccessFile['recommended']) {
                $status[] = 'recommended';
            }

            if (!empty($status)) {
                $status = '(' . implode(', ', $status) . ')';
            } else {
                $status = '';
            }
            $this->output->outputFormatted($i++ . ') %s', array($htaccessFile['file'] . ' ' . $status), 3);
        }
        $this->output->outputLine();

        // Ask which htaccess file to choose
        $amount = count($htaccessFiles);
        $choosenHtaccessIndex = $this->output->askAndValidate(
            'Please choose .htaccess file [0-' . $amount . '] ',
            function ($value) use ($amount) {
                if ($value < 0 || $value > $amount) {
                    throw new \Exception('Please enter a number between 1 and ' . $amount . '!');
                }
                return $value;
            }
        );
        if ($choosenHtaccessIndex === '0') {
            $this->output->outputLine(
                'Skipped. But don\'t forget to set TYPO3_CONTEXT manually. Otherwise frontend will fail.'
            );
            $this->output->outputLine();
            return;
        }

        // Write "SetEnv ..." to at beginning of choosen htaccess file
        $choosenHtaccessFile = $htaccessFiles[$choosenHtaccessIndex - 1];
        $content = '';
        if ($choosenHtaccessFile['existing']) {
            $content = file_get_contents($choosenHtaccessFile['file']);
        }
        $newContent = 'SetEnv TYPO3_CONTEXT ' . $_SERVER['TYPO3_CONTEXT'] . PHP_EOL;
        $status = file_put_contents($choosenHtaccessFile['file'], $newContent . $content);

        // Status output
        $this->output->output('Writing TYPO3_CONTEXT to beginning of "' . $choosenHtaccessFile['file'] . '"... ');
        $this->output->outputLine($status ? 'successful.' : 'failed.');
        $this->output->outputLine();
    }

    protected function checkEnvironmentConfigurationFile()
    {
        $typo3ContextParts = explode('/', $_SERVER['TYPO3_CONTEXT']);
        $defaultEnvFile = array_pop($typo3ContextParts) . '.php';
        $this->userData['environmentConfigFile'] = $this->output->askAndValidate(
            'Which file in typo3conf/Environments/ contains your configuration? [' . $defaultEnvFile . ']: ',
            function ($value) {
                $path = $this->PATH_site . 'typo3conf/Environments/' . $value;
                if (!file_exists($path)) {
                    throw new \Exception('File "' . $path . '" does not exist!');
                }
                return $value;
            },
            false,
            $defaultEnvFile
        );

        $this->output->outputLine();
        $this->output->outputLine();
    }

    protected function checkForDatabaseCredentialsInEnvironmentConfiguration()
    {
        $fileName = $this->userData['environmentConfigFile'];
        $path = $this->PATH_site . 'typo3conf/Environments/' . $fileName;
        $content = file_get_contents($path);
        if (!$content) {
            return;
        }

        preg_match(
            '/setDatabaseCredentials\(.*?\'(.*?)\',.*?\'(.*?)\',.*?\'(.*?)\',.*?\'(.*?)\'.*?\)/s',
            $content,
            $matches
        );
        $user = $matches[1];
        $pass = $matches[2];
        $database = $matches[3];
        $host = $matches[4];

        if (empty($user) || empty($database) || empty($host) ||
            $host === 'host' || $user === 'username' || $pass === 'password') {
            return;
        }

        $this->output->outputLine('Found database credentials in environment configiguration "' . $fileName . '":');
        $this->output->outputTable(array(
            array($host, $user, str_repeat('*', strlen($pass)), $database)
        ), array('Host', 'User', 'Password', 'Database'));

        $this->output->outputLine();
        $useExistingCredentails = $this->output->askConfirmation(
            'Do you want to use these credentials? [Y/n] ',
            true
        );

        if ($useExistingCredentails) {
            $this->userData['host'] = $host;
            $this->userData['user'] = $user;
            $this->userData['pass'] = $pass;
            $this->userData['database'] = $database;
            $this->userData['skipPromptDatabaseCredentials'] = true;
        }
    }

    protected function promptDatabaseCredentials()
    {
        if (array_key_exists('skipPromptDatabaseCredentials', $this->userData)) {
            $this->output->outputLine('Using existing database credentials.');
            $this->output->outputLine();
            return;
        }

        $this->output->outputLine('Please enter MySQL database credentials:');
        $this->output->outputLine();

        $this->userData['host'] = $this->output->ask('Hostname [localhost]: ', 'localhost');
        $this->userData['user'] = $this->output->ask('User: ');
        $this->userData['pass'] = $this->output->askHiddenResponse('Password: ');
        $this->userData['database'] = $this->output->ask('Database: ');
        $this->output->outputLine();
    }

    protected function establishDatabaseConnection()
    {
        $this->output->output('Connecting to database... ');
        try {
            $this->pdo = new \PDO(
                'mysql:dbname=' . $this->userData['database'] . ';host=' . $this->userData['host'],
                $this->userData['user'],
                $this->userData['pass'],
                array(
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                )
            );
        } catch (\Exception $e) {
            $this->output->outputLine('failed!');
            $this->output->outputLine('Error: ' . $e->getMessage());
            die;
        }
        $this->output->outputLine('successful!');
        $this->output->outputLine();
    }

    protected function writeDatabaseCredentialsToEnvironmentConfiguration()
    {
        if (array_key_exists('skipPromptDatabaseCredentials', $this->userData)) {
            return;
        }

        $this->output->output(
            'Updating database credentials in environment config "' . $this->userData['environmentConfigFile'] . '"...'
        );
        $path = $this->PATH_site . 'typo3conf/Environments/' . $this->userData['environmentConfigFile'];
        if (!is_writable($path)) {
            $this->output->outputLine('failed!');
            $this->output->outputLine('File "' . $path . '" not writable!');
            die;
        }

        $content = file_get_contents($path);
        if (strpos('setDatabaseCredentials', $content) !== false) {
            $this->output->outputLine('failed!');
            $this->output->outputLine(
                'Given environment file (' . $this->userData['environmentConfigFile'] . ') must contain the ' .
                'command EnvironmentUtility::setDatabaseCredentials().'
            );
            die;
        }

        $newCommand = "setDatabaseCredentials(
    '" . $this->userData['user'] . "',
    '" . $this->userData['pass'] . "',
    '" . $this->userData['database'] . "',
    '" . $this->userData['host'] . "'
);";

        $updatedContent = preg_replace('/setDatabaseCredentials\(.*?\);/s', $newCommand, $content);
        file_put_contents($path, $updatedContent);
        $this->output->outputLine('successful!');
        $this->output->outputLine();
    }

    protected function checkDatabaseForExistingTables()
    {
        $tablesResult = $this->pdo->query('SHOW TABLES;');
        $tables = $tablesResult->fetchAll();

        if (!empty($tables)) {
            $confirm = $this->output->askConfirmation(
                'The database is not empty! Do you want to proceed and delete all tables? [y/N] ',
                false
            );
            if ($confirm) {
                foreach ($tables as $table) {
                    $this->pdo->exec('DROP TABLE ' . $table[0] . ';');
                }
                $this->output->outputLine('All tables dropped. The database is empty now.');
                $this->output->outputLine();
            } else {
                $this->output->outputLine('Install script aborted.');
                die;
            }
        }
    }

    protected function importBasicSchema()
    {
        $this->output->output('Importing basic schema... ');
        $query = file_get_contents($this->PATH_site . 'typo3/sysext/core/ext_tables.sql');
        $query .= file_get_contents($this->PATH_site . 'typo3/sysext/extensionmanager/ext_tables.sql');
        $query .= file_get_contents($this->PATH_site . 'typo3/sysext/extensionmanager/ext_tables_static+adt.sql');
        $query .= file_get_contents(
            $this->PATH_site . 'typo3conf/ext/iw_base/Resources/Private/Schema/CachingFramework.sql'
        );

        $status = $this->pdo->exec($query);
        if ($status === false) {
            $this->output->outputLine('failed!');
            $errorInfo = $this->pdo->errorInfo();
            $this->output->outputLine($errorInfo[2]);
            die;
        }
        $this->output->outputLine('successful!');
        $this->output->outputLine();
    }

    protected function createCliLowlevelBackendUser()
    {
        $this->output->output('Creating _cli_lowlevel backend user... ');

        $status = $this->pdo->exec('INSERT INTO be_users (username) VALUES ("_cli_lowlevel")');
        if ($status === false) {
            $this->output->outputLine('failed!');
            $errorInfo = $this->pdo->errorInfo();
            $this->output->outputLine($errorInfo[2]);
            die;
        }
        $this->output->outputLine('successful!');
        $this->output->outputLine();
    }

    /**
     * Executes php command and prepends TYPO3_CONTEXT
     *
     * @param string $command
     * @return array Output text and status code
     */
    protected function execCommand($command)
    {
        $cmd = 'TYPO3_CONTEXT=' . $_SERVER['TYPO3_CONTEXT'] . ' ' . PHP_BINARY . ' ' . $command;
        exec($cmd, $output, $statusCode);
        return array(
            implode(PHP_EOL, $output),
            $statusCode
        );
    }

    protected function performUpdateSchema()
    {
        $this->output->output('Calling typo3_console (database::updateschema)... ');
        list($text, $statusCode) = $this->execCommand('typo3cms database:updateschema "*.add,*.change"');
        $this->output->outputLine(($statusCode !== 0) ? 'failed!' : 'succcessful!');
        $this->output->outputLine($text);
        $this->output->outputLine();
    }

    protected function fixFolderStructure()
    {
        $this->output->output('Fixing folder structure... ');
        try {
            define('PATH_site', $this->PATH_site);
            define(
                'TYPO3_OS',
                (!stristr(PHP_OS, 'darwin') && !stristr(PHP_OS, 'cygwin') && stristr(PHP_OS, 'win')) ? 'WIN' : ''
            );
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['fileCreateMask'] = '0664';
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['folderCreateMask'] = '2775';

            /** @var $folderStructureFactory \TYPO3\CMS\Install\FolderStructure\DefaultFactory */
            $folderStructureFactory = new \TYPO3\CMS\Install\FolderStructure\DefaultFactory();
            /** @var $structureFacade \TYPO3\CMS\Install\FolderStructure\StructureFacade */
            $structureFacade = $folderStructureFactory->getStructure();

            $statusUtility = new \TYPO3\CMS\Install\Status\StatusUtility();
            $structureStatus = $structureFacade->getStatus();
            $errors = array_merge(
                $statusUtility->filterBySeverity($structureStatus, 'error'),
                $statusUtility->filterBySeverity($structureStatus, 'warning')
            );
            if (!empty($errors)) {
                $structureFacade->fix();
            }
        } catch (\Exception $e) {
            $this->output->outputLine('failed!');
            $this->output->outputLine($e->getMessage() . ' in ' . $e->getFile() . ' in line ' . $e->getLine());
            die;
        }
        $this->output->outputLine('successful!');
        $this->output->outputLine();
    }

    protected function clearTypo3Caches()
    {
        $this->output->output('Calling typo3_console (cache::flush)... ');
        list($text, $statusCode) = $this->execCommand('typo3cms cache:flush --force');
        $this->output->outputLine(($statusCode !== 0) ? 'failed!' : 'succcessful!');
        $this->output->outputLine($text);
        $this->output->outputLine();
    }

    protected function performT3lephantImport()
    {
        $this->output->output('Perform t3lephant import... ');
        list($text, $statusCode) = $this->execCommand('./typo3/cli_dispatch.phpsh extbase t3lephant:import');
        $this->output->outputLine(($statusCode !== 0) ? 'failed!' : 'succcessful!');
        $this->output->outputLine($text);
        $this->output->outputLine();
    }
}
