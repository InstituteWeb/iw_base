<?php
namespace InstituteWeb\IwBase\Scripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */
use TYPO3\CMS\Extbase\Mvc\Cli\ConsoleOutput;
use TYPO3\CMS\Extbase\Mvc\Exception\CommandException;

/**
 * Compatibility Fix Script
 *
 * On some systems it's not allowed to symlink php files
 * and when suhosin extension is in use, environment variables
 * are not working.
 *
 * @package Iwm\IwBase
 */
class CompatibilityFix
{
    /**
     * @var string Absolute root path of TYPO3
     */
    protected $PATH_site;

    /**
     * @var ConsoleOutput
     */
    protected $output;

    /**
     * @var array Entered user data
     */
    protected $userData = array();

    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * Install constructor.
     *
     * @param string $pathSite Absolute root path of TYPO3. Required.
     * @return Install
     */
    public function __construct($pathSite)
    {
        $this->PATH_site = $pathSite;
        $this->output = new ConsoleOutput();
    }

    /**
     * @param bool $hideHeader
     * @return void
     * @throws CommandException
     */
    public function run($hideHeader = false)
    {
        $step = $this->showMenu($hideHeader);

        switch ($step) {
            case "1":
                $this->taskReplaceSymlinkedIndexFile();
                break;
            case "2":
                $this->taskSetTypo3ContextManually();
                break;
            default:
                $this->output->outputLine('Bye.');
                exit();
        }
    }

    /**
     * Say hello and dispatch task to call
     *
     * @param bool $hideHeader
     * @return string Selected answer
     */
    protected function showMenu($hideHeader = false)
    {
        $this->output->outputLine('');
        if (!$hideHeader) {
            $this->output->outputLine(<<<TXT
+-----------------------------+
|  iw_base Compatibility Fix  |
+-----------------------------+

On some systems it's not allowed to symlink php files
and when suhosin extension is in use, environment variables
are not working.

TXT
            );
        }

        return trim($this->output->ask(
            array(
                'Please choose:' . PHP_EOL,
                '--------------' . PHP_EOL,
                '1) Replace index.php symlink' . PHP_EOL,
                '2) Set TYPO3_CONTEXT manually in source files' . PHP_EOL,
                '0) Exit' . PHP_EOL
            ),
            '1'
        ));
    }


    /**
     * Replaces index.php with typo3_src/index.php.
     * Both files need be existing.
     *
     * @return void
     * @throws CommandException
     */
    protected function taskReplaceSymlinkedIndexFile()
    {
        $path = $this->PATH_site . 'index.php';
        if (!file_exists($path) || !is_writable($path)) {
            throw new CommandException('File not writable: ' . $path);
        }

        $sourcePath = $this->PATH_site . 'typo3_src' . DIRECTORY_SEPARATOR . 'index.php';
        if (!file_exists($sourcePath)) {
            throw new CommandException('File not existing: ' . $sourcePath);
        }

        $proceed = $this->output->askConfirmation('File "' . $path . '" is going to be replaced. Proceed? [Y/n] ');
        if ($proceed) {
            $unlinkStatus = unlink($path);
            if (!$unlinkStatus) {
                throw new CommandException('Deleting failed! File: ' . $path);
            }

            $copyStatus = copy($sourcePath, $path);
            if (!$copyStatus) {
                throw new CommandException('Copying from "' . $sourcePath . '" to "' . $path . '" failed!');
            }
            $this->output->outputLine('index.php has been successfully replaced.');
        } else {
            $this->output->outputLine('Skipped.');
        }

        $this->run(true);
    }


    protected function taskSetTypo3ContextManually()
    {
        $indexFilePath = $this->PATH_site . 'index.php';
        $typo3IndexFilePath = $this->PATH_site . 'typo3_src/typo3/index.php';

        if (!file_exists($indexFilePath) || !is_writable($indexFilePath)) {
            throw new CommandException('File not writable: ' . $indexFilePath);
        }

        if (!file_exists($typo3IndexFilePath) || !is_writable($typo3IndexFilePath)) {
            throw new CommandException('File not writable: ' . $typo3IndexFilePath);
        }

        $environment = $this->output->ask(
            array(
                'Which TYPO3_CONTEXT do you want to apply? [Development/DevServer]' . PHP_EOL,
                '> '
            ),
            'Development/DevServer'
        );
        $putEnvCode = array("putenv('TYPO3_CONTEXT=$environment');");

        $this->applyTypo3ContextToPhpFile($indexFilePath, $putEnvCode);
        $this->applyTypo3ContextToPhpFile($typo3IndexFilePath, $putEnvCode);

        $this->output->outputLine('TYPO3_CONTEXT successfully applied.');
        $this->output->outputLine('');
        $this->output->outputLine('/!\ REMEMBER: When updating TYPO3 to rerun this script again!');
        $this->run(true);
    }

    /**
     * Adds code to second line of given php file.
     * If the second line already starts with "putenv" this line gets removed.
     *
     * @param string $path to file
     * @param array $putEnvCode Code to add to second line of file
     * @throws CommandException
     */
    protected function applyTypo3ContextToPhpFile($path, array $putEnvCode)
    {
        $contentLines = explode(PHP_EOL, file_get_contents($path));
        if (strpos($contentLines[1], 'putenv') === 0) {
            unset($contentLines[1]);
            $contentLines = array_values($contentLines);
        }

        $firstLine = array(array_shift($contentLines));
        $updatedContentLines = array_merge($firstLine, $putEnvCode, $contentLines);

        $status = file_put_contents($path, implode(PHP_EOL, $updatedContentLines));
        if (!$status) {
            throw new CommandException('Updating "' . $path . '" failed.');
        }
    }
}
