<?php
namespace InstituteWeb\IwBase\DomainHandling;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2012-2015 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */

/**
 * Class DomainConfigurationUtility
 * Supports method chaining.
 *
 * @package InstituteWeb\IwBase
 */
class DomainUtility implements \TYPO3\CMS\Core\SingletonInterface
{
    /** Constant for section in $GLOBALS to store settings to */
    const SECTION = 'TYPO3_USER_SETTINGS';

    /** Constant for extension key */
    const EXTKEY = 'iw_base';

    /** Constant for key in array which contains the options for sys_domain record */
    const OPTIONKEY = 'sysDomainOptions';

    /**
     * Add new sys_domain record to environment.
     *
     * @param string $domain The (sub-)domain to add
     * @param int $rootPageUid Page uid of the root page, where new domain should be stored
     * @param array $foreignLanguages Key is language shortcut, value the sys_language_uid (e.g. 'en' => 1).
     *                                The default language must not be configured here.
     * @param bool $isPrimary When true, set forced=1 in sys_domain record
     * @return DomainUtility
     * @throws \Exception
     */
    public function addDomain(
        $domain,
        $rootPageUid,
        array $foreignLanguages = array(),
        $isPrimary = false
    ) {
        self::initializeDomainConfiguration($domain);
        $GLOBALS[self::SECTION][self::EXTKEY]['domainConfigurations'][$domain] = array(
            'rootpageUid' => $rootPageUid,
            'languages' => $foreignLanguages,
            self::OPTIONKEY => ($isPrimary) ? array('forced' => true) : array()
        );
        return $this;
    }

    /**
     * Add new sys_domain_record which is just a redirection
     * or extends existing domain configuration with redirection stuff.
     *
     * @param string $domain The (sub-)domain to add redirection for
     * @param int $rootPageUid Page uid of the root page, where new domain should be stored
     * @param string $redirectTo
     * @param int $redirectHttpStatusCode
     * @param bool $prependParams
     * @param $forced
     * @return DomainUtility
     */
    public function addRedirection(
        $domain,
        $rootPageUid,
        $redirectTo,
        $redirectHttpStatusCode = 301,
        $prependParams = true,
        $forced = false
    ) {
        self::initializeDomainConfiguration($domain);
        $GLOBALS[self::SECTION][self::EXTKEY]['domainConfigurations'][$domain]['rootpageUid'] = $rootPageUid;
        $GLOBALS[self::SECTION][self::EXTKEY]['domainConfigurations'][$domain][self::OPTIONKEY] = array_merge(
            $GLOBALS[self::SECTION][self::EXTKEY]['domainConfigurations'][$domain][self::OPTIONKEY],
            array(
                'redirectTo' => $redirectTo,
                'redirectHttpStatusCode' => $redirectHttpStatusCode,
                'prepend_params' => $prependParams,
                'forced' => $forced
            )
        );
        return $this;
    }

    /**
     * Checks given domainConfiguration and ensure valid base values
     *
     * @param string $domain
     * @return void
     */
    protected static function initializeDomainConfiguration($domain)
    {
        if (!isset($GLOBALS[self::SECTION][self::EXTKEY]['domainConfigurations'])) {
            $GLOBALS[self::SECTION][self::EXTKEY]['domainConfigurations'] = array();
        }
        if (!isset($GLOBALS[self::SECTION][self::EXTKEY]['domainConfigurations'][$domain])) {
            $GLOBALS[self::SECTION][self::EXTKEY]['domainConfigurations'][$domain] = array();
        }
        if (!isset($GLOBALS[self::SECTION][self::EXTKEY]['domainConfigurations'][$domain][self::OPTIONKEY])) {
            $GLOBALS[self::SECTION][self::EXTKEY]['domainConfigurations'][$domain][self::OPTIONKEY] = array();
        }
    }
}
