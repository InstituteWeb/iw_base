<?php
namespace InstituteWeb\IwBase\DomainHandling;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2012-2015 Armin Ruediger Vieweg <vieweg@iwkoeln.de>
 */
use InstituteWeb\IwBase\Utility\DatabaseConnection;

/**
 * Domain Controller
 *
 * @package InstituteWeb\IwBase
 */
class DomainController
{
    /** Constant for filename which contains the realurl configuration md5 hash */
    const CACHE_FILENAME = '.iw_base_sysdomain_hash';

    /** @var array */
    protected $extensionSettings = array();

    /**
     * @var \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected $database;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->database = DatabaseConnection::get();
        $this->extensionSettings = unserialize(
            $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][DomainUtility::EXTKEY]
        );
    }

    /**
     * Dispatches the Dynamic Sysdomains functionality
     *
     * @return void
     */
    public function runAction()
    {
        $realUrlConfiguration = $this->getRealUrlConfiguration();
        $sysDomainTableExists = in_array('sys_domain', array_keys($this->database->admin_get_tables()));
        if (empty($realUrlConfiguration) || !$sysDomainTableExists) {
            return;
        }

        $realUrlConfigurationHash = md5(serialize($realUrlConfiguration));
        if (!$this->getCacheContent() || $this->getCacheContent() != $realUrlConfigurationHash) {
            $this->updateSysDomainEntries($realUrlConfiguration);
            $this->writeCacheContent($realUrlConfigurationHash);
        }

        if ($this->extensionSettings['enableAutoSysDomainForDefault']) {
            $domain = $_SERVER['HTTP_HOST'];
            if (!array_key_exists($domain, $realUrlConfiguration)) {
                $existingDomainRecordCount = $this->database->exec_SELECTcountRows(
                    'domainName',
                    'sys_domain',
                    'domainName="' . $domain . '"'
                );
                if (!$existingDomainRecordCount) {
                    $this->insertDefaultSysDomainEntry($domain, $realUrlConfiguration);
                }
            }
        }
    }

    /**
     * Applies given domain configurations and extends the base realURL configuration.
     * This function must be called at the end of realurl_conf (aka UrlConfiguration.php).
     *
     * @return void
     */
    public function applyDomainConfigurationsAction()
    {
        // Default rootpage uid for automatically added domains
        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['pagePath']['rootpage_id']
            = (int) $this->extensionSettings['defaultRootpageId'];

        if (is_array($GLOBALS[DomainUtility::SECTION][DomainUtility::EXTKEY]['domainConfigurations'])) {
            foreach ($GLOBALS[DomainUtility::SECTION][DomainUtility::EXTKEY]['domainConfigurations'] as $domain => $configuration) {
                $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domain] = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT'];
                $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domain]['preVars']['L']['valueMap'] = $configuration['languages'];
                $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domain]['pagePath']['rootpage_id'] = $configuration['rootpageUid'];
                $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domain]['pagePath'][DomainUtility::OPTIONKEY]
                    = $configuration[DomainUtility::OPTIONKEY];
            }

            if (isset($_SERVER['REQUEST_URI']) && isset($_SERVER['HTTP_HOST'])) {
                $languageKey = substr($_SERVER['REQUEST_URI'], 1, 2);
                if ($languageKey && array_key_exists($_SERVER['HTTP_HOST'], $GLOBALS[DomainUtility::SECTION][DomainUtility::EXTKEY]['domainConfigurations'])) {
                    $configuration = $GLOBALS[DomainUtility::SECTION][DomainUtility::EXTKEY]['domainConfigurations'][$_SERVER['HTTP_HOST']];
                    if (array_key_exists($languageKey, $configuration['languages'])) {
                        $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling']
                            = $languageKey . '/' . $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'];
                    }
                }
            }
        }
    }

    /**
     * Updates all sys_domain entries with configured domains and attributes
     * from realURL configuration.
     *
     * @param array $realUrlConfiguration
     * @return bool
     */
    protected function updateSysDomainEntries(array $realUrlConfiguration)
    {
        $insertRows = array();
        foreach ($realUrlConfiguration as $domain => $configuration) {
            if ($domain === '_DEFAULT') {
                continue;
            }
            $insertRows[] = $this->buildInsertRow($domain, $configuration);
        }
        if (count($insertRows) > 0) {
            return $this->executeInsertQuery($insertRows);
        }
        return false;
    }

    /**
     * Inserts one sys_domain entry, based on given $domain and _DEFAULT realURL configuration
     *
     * @param string $domain
     * @param array $realUrlConfiguration
     * @return bool
     */
    protected function insertDefaultSysDomainEntry($domain, array $realUrlConfiguration)
    {
        $defaultConfiguration = $realUrlConfiguration['_DEFAULT'];

        $insertRows = array($this->buildInsertRow($domain, $defaultConfiguration));
        return $this->executeInsertQuery($insertRows, true);
    }

    /**
     * Executes an insert query and truncates the sys_domain table
     *
     * @param array $insertRows Rows to insert to sys_domain table
     * @param bool $doNotTruncate If TRUE the table sys_domain will not be truncated
     * @return bool If inserted queries were successful returns TRUE
     */
    protected function executeInsertQuery(array $insertRows, $doNotTruncate = false)
    {
        if ($doNotTruncate === false) {
            $this->database->exec_TRUNCATEquery('sys_domain');
        }
        foreach ($insertRows as $insertRow) {
            $status = $this->database->exec_INSERTquery(
                'sys_domain',
                $insertRow
            );
            if (!$status) {
                return false;
            }
        }
        return true;
    }

    /**
     * Builds a row array out of given domain and realurl configuration
     * for given domain. Removes keys with NULL as value.
     *
     * @param string $domain
     * @param array $configuration
     * @return array
     */
    protected function buildInsertRow($domain, array $configuration)
    {
        $insertRow = $configuration;

        $insertRow['pid'] = $configuration['rootpage_id'];
        unset($insertRow['rootpage_id']);

        if ($this->extensionSettings['useRootpageIdAsUidOfDomainRecord']) {
            $insertRow['uid'] = $insertRow['pid'];
        }
        $insertRow['domainName'] = $domain;
        $insertRow['tstamp'] = time();
        $insertRow['crdate'] = time();

        $realInsertRow = array();
        foreach ($insertRow as $column => $value) {
            if ($value !== null) {
                $realInsertRow[$column] = $value;
            }
        }
        return $realInsertRow;
    }

    /**
     * Reads cache content. Returns FALSE if file does not exist
     *
     * @return bool|string
     */
    protected function getCacheContent()
    {
        $path = PATH_site . 'typo3temp/' . self::CACHE_FILENAME;
        if (file_exists($path)) {
            return file_get_contents($path);
        }
        return false;
    }

    /**
     * Writes content to cache file
     *
     * @param string $content
     * @return void
     */
    protected function writeCacheContent($content)
    {
        \TYPO3\CMS\Core\Utility\GeneralUtility::writeFileToTypo3tempDir(
            PATH_site . 'typo3temp/' . self::CACHE_FILENAME,
            $content
        );
    }

    /**
     * Gets the realURL configuration for this extension.
     *
     * @return array
     */
    protected function getRealUrlConfiguration()
    {
        $originalRealUrlConfiguration = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'];

        $realUrlConfiguration = array();
        foreach ($originalRealUrlConfiguration as $domain => $configuration) {
            $configurationEntry = array(
                'rootpage_id' => null,
                'redirectTo' => null,
                'redirectHttpStatusCode' => null,
                'prepend_params' => null,
                'forced' => null,
            );

            if (isset($configuration['pagePath']['rootpage_id'])) {
                $configurationEntry['rootpage_id'] = $configuration['pagePath']['rootpage_id'];
            }

            if (isset($configuration['pagePath'][DomainUtility::OPTIONKEY]['redirectTo'])
                && isset($configuration['pagePath'][DomainUtility::OPTIONKEY]['redirectHttpStatusCode'])) {
                // Redirect to and HTTP Status Code for the Redirect
                $configurationEntry['redirectTo'] = $configuration['pagePath'][DomainUtility::OPTIONKEY]['redirectTo'];
                $configurationEntry['redirectHttpStatusCode'] =
                    $configuration['pagePath'][DomainUtility::OPTIONKEY]['redirectHttpStatusCode'];
            }
            if (isset($configuration['pagePath'][DomainUtility::OPTIONKEY]['prepend_params'])) {
                // Transfer parameters to Redirect URL
                $configurationEntry['prepend_params'] = $configuration['pagePath'][DomainUtility::OPTIONKEY]['prepend_params'];
            }
            if (isset($configuration['pagePath'][DomainUtility::OPTIONKEY]['forced'])) {
                // Always prepend this domain in links:
                $configurationEntry['forced'] = $configuration['pagePath'][DomainUtility::OPTIONKEY]['forced'];
            }

            if (isset($configurationEntry['rootpage_id'])) {
                $realUrlConfiguration[$domain] = $configurationEntry;
            }
        }
        return $realUrlConfiguration;
    }
}
